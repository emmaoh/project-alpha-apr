from django.views.generic import CreateView, ListView, UpdateView
from tasks.models import Task
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django import template

register = template.Library()


class TaskCreateView(LoginRequiredMixin, CreateView):
    model = Task
    template_name = "tasks/create.html"
    fields = ["name", "start_date", "due_date", "project", "assignee"]

    def get_success_url(self) -> str:
        return reverse_lazy("show_project", args=[self.object.project.id])


class TaskListView(LoginRequiredMixin, ListView):
    model = Task
    template_name = "tasks/list.html"

    def get_queryset(self):
        return Task.objects.filter(assignee=self.request.user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["percentage_done"] = (
            Task.objects.filter(
                is_completed=True, assignee=self.request.user
            ).count()
            * 100
            / Task.objects.filter(assignee=self.request.user).count()
        )
        return context


class TaskUpdateView(LoginRequiredMixin, UpdateView):
    model = Task
    fields = ["is_completed"]

    def get_success_url(self) -> str:
        return reverse_lazy("show_my_tasks")
